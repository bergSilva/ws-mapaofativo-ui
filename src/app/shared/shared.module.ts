import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { ModuleWithProviders } from '@angular/compiler/src/core';




@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule
  ],
})
export class SharedModule { 
  static forRoot(): ModuleWithProviders{
    return{
      ngModule:SharedModule,
      providers:[
        
      ]
    }
  }
}
