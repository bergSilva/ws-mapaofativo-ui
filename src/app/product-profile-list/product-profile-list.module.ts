import { NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ProductProfileListComponent } from './product-profile-list.component';
import { SharedModule } from '../shared/shared.module';
import {MatToolbarModule} from '@angular/material/toolbar';


const route: Routes= [
  {path:'', component: ProductProfileListComponent}
]
@NgModule({

  declarations: [ProductProfileListComponent],
  exports:[
    MatToolbarModule
  ],
  imports: [
    CommonModule,
    SharedModule,
    MatToolbarModule,
    RouterModule.forChild(route)
  ]
})
export class ProductProfileListModule { }
