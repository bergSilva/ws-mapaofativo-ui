import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductProfileListComponent } from './product-profile-list.component';

describe('ProductProfileListComponent', () => {
  let component: ProductProfileListComponent;
  let fixture: ComponentFixture<ProductProfileListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductProfileListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductProfileListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
